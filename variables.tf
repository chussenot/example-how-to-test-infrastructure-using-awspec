variable "aws_region" {
  default = "eu-west-1"
}

variable "aws_availability_zone" {
  default = "a"
}
