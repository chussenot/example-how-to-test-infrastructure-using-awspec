# encoding: utf-8
# copyright: 2018, Hugo Paredes

# frozen_string_literal: true

require 'awspec'
require 'aws-sdk'

state_file = 'terraform.tfstate.d/kitchen-terraform-default-aws/terraform.tfstate'
tf_state = JSON.parse(File.open(state_file).read)

instance_id = tf_state['modules'][0]['outputs']['ec2_instance.database.id']['value']
sg_db_id = tf_state['modules'][0]['outputs']['security_group.pgsql.id']['value']
sg_web_id = tf_state['modules'][0]['outputs']['security_group.webserver.id']['value']
vpc_id = tf_state['modules'][0]['outputs']['vpc.id']['value']
subnet_id = tf_state['modules'][0]['outputs']['subnet.private.id']['value']


describe ec2(instance_id) do
  it { should exist }
  it { should be_running }
  it { should have_security_group(sg_db_id) }
  it { should belong_to_vpc(vpc_id) }
  it { should belong_to_subnet(subnet_id) }
end

describe security_group(sg_db_id) do
  it { should exist }

  its(:inbound_permissions_count) { should eq 1 }
  its(:inbound) { should be_opened(5432).protocol('tcp').for(sg_web_id) }

  its(:outbound_permissions_count) { should eq 1 }
  its(:outbound) { should be_opened(0).protocol('tcp').for('0.0.0.0/0') }
end
