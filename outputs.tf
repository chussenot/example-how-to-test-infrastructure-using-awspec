output "ami.image_id" {
  value = "${data.aws_ami.ubuntu.id}"
}

output "ec2_instance.webserver.id" {
  value = "${aws_instance.webserver.id}"
}

output "ec2_instance.webserver.name" {
  value = "${aws_instance.webserver.tags.Name}"
}

output "ec2_instance.webserver.ami" {
  value = "${aws_instance.webserver.ami}"
}

output "ec2_instance.webserver.instance_type" {
  value = "${aws_instance.webserver.instance_type}"
}

output "ec2_instance.webserver.public_ip" {
  value = "${aws_instance.webserver.public_ip}"
}

output "ec2_instance.database.name" {
  value = "${aws_instance.database.tags.Name}"
}

output "ec2_instance.database.id" {
  value = "${aws_instance.database.id}"
}

output "ec2_instance.database.ami" {
  value = "${aws_instance.database.ami}"
}

output "ec2_instance.database.instance_type" {
  value = "${aws_instance.database.instance_type}"
}

output "ec2_instance.database.private_ip" {
  value = "${aws_instance.database.private_ip}"
}

output "vpc.id" {
  value = "${aws_vpc.default.id}"
}

output "subnet.public.id" {
  value = "${aws_subnet.public.id}"
}

output "subnet.private.id" {
  value = "${aws_subnet.private.id}"
}

output "security_group.webserver.id" {
  value = "${aws_security_group.webserver.id}"
}

output "security_group.webserver.name" {
  value = "${aws_security_group.webserver.name}"
}

output "security_group.pgsql.id" {
  value = "${aws_security_group.pgsql.id}"
}

output "security_group.pgsql.name" {
  value = "${aws_security_group.pgsql.name}"
}

output "route.internet_access.id" {
  value = "${aws_route.internet_access.route_table_id}"
}
